module.exports = function () {
    return {
        environment: process.env.NODE_ENV || "development",
        server:{
            port: process.env.SERVER_PORT || 3000
        },
        modes: {
            development:'development',
            production:'production'
        },
        images:{
            uploadDir: __dirname + '/uploads/',
            allowedFormats:['png'],
            maximumImageDimentionSize:400,
            maximumFileSize:"10mb"
        },
        mongo:{
            database: process.env.MONGO_CONNECTION || null
        }
    }
}();
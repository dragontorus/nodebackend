let mongoose = require('mongoose');
let Schema = mongoose.Schema;
const IMAGES_CONFIG =  require('../../config').images;

const Image = new Schema({
    host: {
        type: String,
        required: [true, '"host" is missing']
    },
    name: {
        type: String,
        validate: {
            validator: (value) => {
                return value.length>0;
            },
            message: '"name" must have length of minimum 1 character'
        },
    },
    fsName: {
        type: String,
        validate: {
            validator: (value) => {
                return value.length>0;
            },
            message: '"fsName" must have length of minimum 1 character'
        },
    },
    format: {
        type: String,
        enum: IMAGES_CONFIG.allowedFormats,
        required: [true, '"format" is missing']
    }
},{
    toObject: {
        transform: (doc, ret) =>{
            ret.url = ret.host + '/image/' + ret._id;
            delete ret.__v;
            delete ret.fsName;
            return ret;
        }
    }
});

module.exports = Image;
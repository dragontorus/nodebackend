let mongoose = require('mongoose')
    , Schema = mongoose.Schema;
const image = require('./image.schema');
const Entity = new Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        auto: true,
    },
    name: {
        type: String,
        unique: true,
        validate: {
            validator: (value) => {
                return value.length>0 && value.length<64;
            },
            message: '"name" must have length from 0 to 64 characters'
        }
    },
    x: {
        type: Number,
        min: [1, '"x" coordinate must be positive number'],
        required: [true, '"x" coordinate is missing']
    },
    y: {
        type: Number,
        min: [1, '"y" coordinate must be positive number'],
        required: [true, '"y" coordinate is missing']
    },
    picture: image,
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        default: Date.now
    }
},{
    toObject: {
        transform: (doc, ret) => {
            delete ret.__v;
        }
    }
});

module.exports = Entity;
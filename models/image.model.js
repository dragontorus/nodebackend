let mongoose = require('mongoose');
let schema = require('./schemas/image.schema');
const MODEL = 'Image';

module.exports  = mongoose.model(MODEL, schema);
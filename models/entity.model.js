let mongoose = require('mongoose');
let schema = require('./schemas/entity.schema');
const timestamp = require('./plugins/timestamps');
const MODEL = 'Entity';
const IMAGES_CONFIG =  require('../config').images;

schema.virtual('url').get(function() {
    return this.picture.host + '/' + this.picture.name;
});

schema.virtual('diskPath').get(function () {
    return IMAGES_CONFIG.uploadDir + this.picture.fsName + '.' + this.picture.format;
});

schema.plugin(timestamp);

module.exports  = mongoose.model(MODEL, schema);
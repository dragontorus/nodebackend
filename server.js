const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const config = require('./config');
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json({limit: '1mb'}));

mongoose.connect(config.mongo.database,{useNewUrlParser: true},
    (err) => {
        if (err) throw err;
        console.log('Successfully connected');
    }
);
mongoose.set('useCreateIndex', true);

require('./routes/index.routes')(app);
require('./routes/entity.routes')(app);
require('./routes/image.routes')(app);

app.listen(config.server.port, function () {
    console.log('App listening on port ' + config.server.port);
});
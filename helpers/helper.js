const config =  require('../config');

exports.getUrlBaseFromRequest = function (request) {
    return request.protocol + "://" + request.headers.host;
};

exports.errorResponseHandler = function (error, res) {

    switch (error.name) {
        case 'ValidationError':
            return res.status(422).json({message:error.message});
        case 'CastError':
            return res.status(422).json({message:error.message});
        case 'DocumentNotFoundError':
            return res.status(404).json({message:error.message});
        case 'RmError':
            return res.status(error.status).json({
                message:(config.modes.development === config.environment)? stack : undefined
            });
        default:
            return res.status(500).json({message:error.message});
    }
};

function RmError (msg, name, status, stack) {
    this.message = msg;
    this.name = name || 'RmError';
    this.status = status || 500;
    this.stack = (config.modes.development === config.environment)?stack : undefined;
    Error.call(this, msg);

}
RmError.prototype.__proto__ = Error.prototype;
exports.RmError = RmError;
const multer = require('multer');
const sharp = require('sharp');
const IMAGES_CONFIG =  require('../config').images;
const {RmError,errorResponseHandler} = require('../helpers/helper');

exports.uploadImage = function (req, res, next) {
    let upload = multer({storage: multer.memoryStorage()}).single('picture');
    upload(req, res, function (err) {
        if (err) {
            return errorResponseHandler(new RmError("File not found in request", 'BadFound', 400),res);
        }
        return next();
    });
};

exports.getImageInfo = async function (sharpImageStream) {
    return await sharpImageStream.metadata();
};

exports.prepareImageForStorage = function (buffer) {
    const image = sharp(buffer);
    return image.resize(IMAGES_CONFIG.maximumImageDimentionSize, IMAGES_CONFIG.maximumImageDimentionSize).max();
};
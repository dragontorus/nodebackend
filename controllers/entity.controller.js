const EntityModel = require('../models/entity.model');
const ImageModel = require('../models/image.model');
const {getUrlBaseFromRequest, errorResponseHandler, RmError} = require('../helpers/helper');


exports.createEntity = async function(req, res) {
    try {
        let image = await ImageModel.find();
        let entity = await EntityModel.findOneAndUpdate({'name':req.body.name}, req.body, {upsert:true,new: true,runValidators: true, setDefaultsOnInsert: true});

        if(!entity){
            throw new RmError("Entity Not found", 'NotFound', 404);
        }
        res.json(entity);
    } catch (e) {
        return errorResponseHandler(e,res);
    }
};

exports.getById = async function(req, res) {
    try {
        let entity = await EntityModel.findById(req.params.id);
        if(!entity){
            throw new RmError("Entity Not found", 'NotFound', 404);
        }
        res.json(entity);
    } catch (e) {
        return errorResponseHandler(e,res);
    }

};

exports.getAllEntities = async function(req, res) {
    try {
        let entities = await EntityModel.find();
        if(!entities){
            throw new RmError("Entity Not found", 'NotFound', 404);
        }
        res.json(entities);
    } catch (e) {
        return errorResponseHandler(e,res);
    }
};
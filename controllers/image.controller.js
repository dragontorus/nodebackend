let fs = require('fs');
let uuid = require('uuid/v4');
let mime = require('mime');

const ImageModel = require('../models/image.model');
const EntityModel = require('../models/entity.model');
const IMAGES_CONFIG =  require('../config').images;
const {prepareImageForStorage,getImageInfo} = require('../services/imageUpload');
const {getUrlBaseFromRequest, errorResponseHandler, RmError} = require('../helpers/helper');

exports.getImageByName = async function(req, res) {
    try{
        let entity = await EntityModel.findOne({ 'picture._id': req.params.id });
        if(!entity){
            throw new RmError("Picture Not found", 'NotFound', 404);
        }

        let file = entity.diskPath;

        let mimetype = mime.lookup(file);

        res.setHeader('Content-disposition', 'attachment; filename=' + entity.picture.name + '.' + entity.picture.format);
        res.setHeader('Content-type', mimetype);

        let filestream = fs.createReadStream(file);
        filestream.pipe(res);
    } catch (e) {
        return errorResponseHandler(e,res);
    }
};

exports.attachImageToEntity = async function(req, res) {
    try {
        let entity = await EntityModel.findById(req.params.id);
        if(!entity){
            throw new RmError("Entity Not found", 'NotFound', 404);
        }
        let filePathName = uuid();
        let file = fs.createWriteStream(IMAGES_CONFIG.uploadDir + filePathName + "." + req.file.mimetype.split('/')[1], {flags: "wx"});
        let imageStream = prepareImageForStorage(req.file.buffer);
        let imageInfo = await getImageInfo(imageStream);
        imageStream.pipe(file);

        entity.picture = new ImageModel({
            host:getUrlBaseFromRequest(req),
            name:req.file.originalname.split('.')[0],
            fsName:filePathName,
            format: imageInfo.format
        });

        await entity.save();
        res.json(entity.picture);
    } catch (e) {
        return errorResponseHandler(e,res);
    }
};
const assert = require('assert');
const request = require('request-promise-native');
const config = require('../config');
const testHost = 'http://localhost:8000' ;
const _ = require('underscore');
const fs = require('fs');

describe('Test Endpoints (Valid scenarios)', function() {
    let entity = {};
    let image = {};
    it('It should create the entity', async function() {
        let optionsApi = {
            url: testHost + '/entity',
            headers: {
                'User-Agent': 'Express-Agent'
            },
            method:"POST",
            body: {
                name: 'testName',
                x:11,
                y:22
            },
            json: true
        };

        let resp = await request(optionsApi);
        assert.ok(resp._id);
        assert.equal(resp.name, "testName");
        assert.equal(resp.x, 11);
        assert.equal(resp.y, 22);
        entity = resp;
    });
    it('It should create the entity without "name"', async function() {
        let optionsApi = {
            url: testHost + '/entity',
            headers: {
                'User-Agent': 'Express-Agent'
            },
            method:"POST",
            body: {
                x:111,
                y:222
            },
            json: true
        };

        let resp = await request(optionsApi);
        assert.ok(resp._id);
        assert.equal(resp.name, null);
        assert.equal(resp.x, 111);
        assert.equal(resp.y, 222);
        entity = resp;
    });
    it('It should return entity by id', async function() {
        let optionsApi = {
            url: testHost + '/entity/'+ entity._id,
            headers: {
                'User-Agent': 'Express-Agent'
            },
            json: true
        };

        let resp = await request(optionsApi);
        assert.equal(resp._id, entity._id);
        assert.equal(resp.name, entity.name);
        assert.equal(resp.x, entity.x);
        assert.equal(resp.y, entity.y);
    });
    it('It should update the entity to x-22 and y-33', async function() {
        let optionsApi = {
            url: testHost + '/entity',
            headers: {
                'User-Agent': 'Express-Agent'
            },
            method:"POST",
            body: {
                name: 'testName',
                x:22,
                y:33
            },
            json: true
        };

        let resp = await request(optionsApi);
        assert.ok(resp._id);
        assert.equal(resp.name, "testName");
        assert.equal(resp.x, 22);
        assert.equal(resp.y, 33);
        entity = resp;
    });
    it('It should update the entity to x-1 and y-2', async function() {
        let optionsApi = {
            url: testHost + '/entity',
            headers: {
                'User-Agent': 'Express-Agent'
            },
            method:"POST",
            body: {
                name: 'testName2',
                x:1,
                y:2
            },
            json: true
        };

        let resp = await request(optionsApi);
        assert.ok(resp._id);
        assert.equal(resp.name, "testName2");
        assert.equal(resp.x, 1);
        assert.equal(resp.y, 2);
        entity = resp;
    });
    it('It should return array entities', async function() {
        let optionsApi = {
            url: testHost + '/entity',
            headers: {
                'User-Agent': 'Express-Agent'
            },
            json: true
        };

        let resp = await request(optionsApi);
        assert.ok(resp.length>0);
    });
    it('It should upload image and attach to entity', async function() {
        let optionsApi = {
            method: 'POST',
            url: testHost + '/image/entity/' + entity._id,
            formData: {
                name: 'picture',
                picture: fs.createReadStream('tests/image.png')
            },
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            json: true
        };
        let resp = await request(optionsApi);
        assert.ok(resp._id);
        assert.ok(resp.host.indexOf("http://localhost")>-1);
        assert.equal(resp.format, "png");
        assert.ok(resp.url.indexOf("http://localhost")>-1);
        image = resp;
    });
    it('It should return entity object with picture object by id', async function() {
        let optionsApi = {
            url: testHost + '/entity/'+ entity._id,
            headers: {
                'User-Agent': 'Express-Agent'
            },
            json: true
        };

        let resp = await request(optionsApi);
        assert.equal(resp._id, entity._id);
        assert.equal(resp.name, entity.name);
        assert.equal(resp.x, entity.x);
        assert.equal(resp.y, entity.y);
        assert.ok(resp.picture);
        assert.equal(resp.picture.host, image.host);
        assert.equal(resp.picture.name, image.name);
        assert.equal(resp.picture.format, image.format);
    });
});

describe('Test Endpoints (InValid scenarios)', function() {
    it('It should respond with 422 status for "y"', async function() {
        let optionsApi = {
            url: testHost + '/entity',
            headers: {
                'User-Agent': 'Express-Agent'
            },
            method:"POST",
            body: {
                name: 'testName',
                x:11,
                y:-1
            },
            json: true
        };
        try {
             await request(optionsApi);
        } catch (e) {
            assert.equal(e.error.message, 'Validation failed: y: "y" coordinate must be positive number');
        }
    });
    it('It should respond with 422 status for "x"', async function() {
        let optionsApi = {
            url: testHost + '/entity',
            headers: {
                'User-Agent': 'Express-Agent'
            },
            method:"POST",
            body: {
                name: 'testName',
                x:-22,
                y:11
            },
            json: true
        };
        try {
            let resp = await request(optionsApi);
            assert.ok(!resp);
        } catch (e) {
            assert.equal(e.error.message, 'Validation failed: x: "x" coordinate must be positive number');
        }
    });

    it('It should return error for entity request by id', async function() {
        let optionsApi = {
            url: testHost + '/entity/111',
            headers: {
                'User-Agent': 'Express-Agent'
            },
            json: true
        };

        try {
            let resp = await request(optionsApi);
            assert.ok(!resp);
        } catch (e) {
            assert.equal(e.error.message, 'Cast to ObjectId failed for value "111" at path "_id" for model "Entity"');
        }
    });

    it('It should return not found error for entity request by id', async function() {
        let optionsApi = {
            url: testHost + '/entity/5b865f160430e946b6d00a13',
            headers: {
                'User-Agent': 'Express-Agent'
            },
            json: true
        };

        try {
            let resp = await request(optionsApi);
            assert.ok(!resp);
        } catch (e) {
            assert.equal(e.error.message, 'Entity Not found');
        }
    });


    it('It should not upload image returning not found error', async function() {
        let optionsApi = {
            method: 'POST',
            url: testHost + '/image/entity/5b865f160430e946b6d00a13',
            formData: {
                name: 'picture',
                picture: fs.createReadStream('tests/image.png')
            },
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            json: true
        };
        try {
            let resp = await request(optionsApi);
            assert.ok(!resp);
        } catch (e) {
            assert.equal(e.error.message, 'Entity Not found');
        }
    });
    it('It should not upload image returning not bad request error', async function() {
        let optionsApi = {
            method: 'POST',
            url: testHost + '/image/entity/5b865f160430e946b6d00a13',
            formData: {
                name: 'none',
                none: fs.createReadStream('tests/image.png')
            },
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            json: true
        };
        try {
            let resp = await request(optionsApi);
            assert.ok(!resp);
        } catch (e) {
            assert.equal(e.error.message, 'File not found in request');
        }
    });
});
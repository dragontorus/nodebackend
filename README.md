# nodeBackEnd


## Installation

Clone the repo:

    1. git clone git@bitbucket.org:dragontorus/nodebackend.git
    2. cd nodebackend
    3. git checkout dev

To start the server please run following commands in terminal:

    1. npm install
    2. sudo npm install pm2 -g
    3. pm2 start pm2config.json --env=development
       
Extra: 

    1. You can configure environment variables in pm2config.json
    2. use npm test to run tests


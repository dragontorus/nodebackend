const controller = require('../controllers/entity.controller');

module.exports = (app) =>{
    app.get('/entity', controller.getAllEntities);
    app.get('/entity/:id', controller.getById);
    app.post('/entity', controller.createEntity);
};
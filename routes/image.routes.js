const ImageController = require('../controllers/image.controller');
const {uploadImage} = require('../services/imageUpload');
module.exports = (app) => {
    app.get('/image/:id', ImageController.getImageByName);
    app.post('/image/entity/:id', uploadImage, ImageController.attachImageToEntity);
};